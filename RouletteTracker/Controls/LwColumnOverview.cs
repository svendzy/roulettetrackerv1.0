﻿using System.Windows.Forms;

namespace RouletteTracker.Controls
{
    public class LwColumnOverview : ListView
    {
        public LwColumnOverview()
        {
            this.SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
        }
    }
}
