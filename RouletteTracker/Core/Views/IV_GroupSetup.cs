﻿using System;
using RouletteTracker.Core.Objects;

namespace RouletteTracker.Core.Views
{
    public interface IV_GroupSetup : IView
    {
        NumberGroups NumberGroups { get; set; }
        void Display();
        event EventHandler Save;
    }
}
