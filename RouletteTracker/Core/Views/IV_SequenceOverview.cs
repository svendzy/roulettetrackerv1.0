﻿using System.Collections.Generic;

namespace RouletteTracker.Core.Views
{
    public interface IV_SequenceOverview : IView
    {
        IList<int> NumberSequence { set; }
        void AddNumber(int number);
        void ResetView();
        event AddSpinEventHandler AddSpin;
    }
}
