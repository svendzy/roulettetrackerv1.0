﻿using System;
using System.Runtime.Remoting;
using RouletteTracker.Core;
using RouletteTracker.Models;

namespace RouletteTracker.Factories
{
    public static class ModelFactory
    {
        // Create model based on type
        // For reduced binding and configurable models, we can later use Activator.CreateInstance
        public static IRouletteTrackerModel CreateModel(ModelType modelType)
        {
            IRouletteTrackerModel model;
            switch (modelType)
            {
                case ModelType.File:
                    model = new RouletteTrackerFileModel();
                    break;
                    default:
                    model = new RouletteTrackerDbModel();
                    break;
            }
            return model;
        }
    }
}
