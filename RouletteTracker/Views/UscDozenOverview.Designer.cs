﻿using RouletteTracker.Controls;

namespace RouletteTracker.Views
{
    partial class UscDozenOverview
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lwDozenOverview = new LwDozenOverview();
            this.colDozen1_12 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colDozen13_24 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colDozen25_36 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.SuspendLayout();
            // 
            // lwDozenOverview
            // 
            this.lwDozenOverview.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colDozen1_12,
            this.colDozen13_24,
            this.colDozen25_36});
            this.lwDozenOverview.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lwDozenOverview.GridLines = true;
            this.lwDozenOverview.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.lwDozenOverview.Location = new System.Drawing.Point(0, 0);
            this.lwDozenOverview.MultiSelect = false;
            this.lwDozenOverview.Name = "lwDozenOverview";
            this.lwDozenOverview.Size = new System.Drawing.Size(201, 313);
            this.lwDozenOverview.TabIndex = 39;
            this.lwDozenOverview.UseCompatibleStateImageBehavior = false;
            this.lwDozenOverview.View = System.Windows.Forms.View.Details;
            // 
            // colDozen1_12
            // 
            this.colDozen1_12.Text = "1-12";
            this.colDozen1_12.Width = 64;
            // 
            // colDozen13_24
            // 
            this.colDozen13_24.Text = "13-24";
            this.colDozen13_24.Width = 64;
            // 
            // colDozen25_36
            // 
            this.colDozen25_36.Text = "25-36";
            this.colDozen25_36.Width = 64;
            // 
            // UscDozenOverview
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lwDozenOverview);
            this.DoubleBuffered = true;
            this.Name = "UscDozenOverview";
            this.Size = new System.Drawing.Size(201, 313);
            this.ResumeLayout(false);

        }

        #endregion

        private LwDozenOverview lwDozenOverview;
        private System.Windows.Forms.ColumnHeader colDozen1_12;
        private System.Windows.Forms.ColumnHeader colDozen13_24;
        private System.Windows.Forms.ColumnHeader colDozen25_36;
    }
}
