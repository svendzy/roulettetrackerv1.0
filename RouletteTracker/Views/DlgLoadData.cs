﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using RouletteTracker.Core.Views;

namespace RouletteTracker.Views
{
    public partial class DlgLoadData : Form, IV_LoadData
    {
        private string _selectedDataDescriptor;

        public DlgLoadData()
        {
            InitializeComponent();
        }

        public IList<string> DataDescriptors
        {
            set
            {
                lbDataDescriptors.BeginUpdate();
                lbDataDescriptors.Items.Clear();
                for (int nDescriptor = 0; nDescriptor < value.Count; nDescriptor++)
                {
                    lbDataDescriptors.Items.Add(value[nDescriptor]);
                }
                lbDataDescriptors.EndUpdate();

                _selectedDataDescriptor = null;
            }
        }

        public string SelectedDataDescriptor
        {
            get { return _selectedDataDescriptor; }
        }
        
        public void Display()
        {
            ShowDialog();
        }

        public event EventHandler LoadData
        {
            add { btnLoad.Click += value; }
            remove { btnLoad.Click -= value; }
        }

        private void lbDataDescriptors_SelectedValueChanged(object sender, EventArgs e)
        {
            if (lbDataDescriptors.SelectedItem != null)
                _selectedDataDescriptor = (string)lbDataDescriptors.SelectedItem;
        }
    }
}
