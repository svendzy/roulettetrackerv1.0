﻿using System.Collections.Generic;
using System.Windows.Forms;
using RouletteTracker.Core.Views;

namespace RouletteTracker.Views
{
    public partial class UscColumnOverview : UserControl, IV_ColumnOverview
    {
        public UscColumnOverview()
        {
            InitializeComponent();
        }

        public IList<int> NumberSequence
        {
            set
            {
                ResetView();
                DrawNumberSequence(value);
            }
        }

        public void AddNumber(int number)
        {
            lwColumnOverview.BeginUpdate();
            DrawNumber(number);
            lwColumnOverview.EndUpdate();
        }

        public void DrawNumberSequence(IList<int> numberSequence)
        {
            lwColumnOverview.BeginUpdate();
            for (int n = 0; n < numberSequence.Count; n++)
            {
                DrawNumber(numberSequence[n]);
            }
            lwColumnOverview.EndUpdate();
        }

        private void DrawNumber(int number)
        {
            if (number == 0) return;

            ListViewItem lwItem = new ListViewItem();

            int numberCol = ((number % 3) == 0) ? 3 : (number % 3);

            switch (numberCol)
            {
                case 1:
                    lwItem.Text = "X";
                    lwItem.SubItems.Add("");
                    lwItem.SubItems.Add("");
                    break;
                case 2:
                    lwItem.Text = "";
                    lwItem.SubItems.Add("X");
                    lwItem.SubItems.Add("");
                    break;
                case 3:
                    lwItem.Text = "";
                    lwItem.SubItems.Add("");
                    lwItem.SubItems.Add("X");
                    break;
            }
            if (lwColumnOverview.Items.Count == 0)
                lwColumnOverview.Items.Add(lwItem);
            else
                lwColumnOverview.Items.Insert(0, lwItem);
        }

        public void ResetView()
        {
            lwColumnOverview.BeginUpdate();
            lwColumnOverview.Items.Clear();
            lwColumnOverview.EndUpdate();
        }

        
    }
}
