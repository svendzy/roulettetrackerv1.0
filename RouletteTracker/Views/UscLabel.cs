﻿using System.Windows.Forms;

namespace RouletteTracker.Views
{
    public partial class UscLabel : UserControl
    {
        public UscLabel()
        {
            InitializeComponent();
        }

        public string LabelText
        {
            get { return lblText.Text; }
            set { lblText.Text = value; }
        }

        public string Value
        {
            get { return lblValue.Text; }
            set { lblValue.Text = value; }
        }
    }
}
