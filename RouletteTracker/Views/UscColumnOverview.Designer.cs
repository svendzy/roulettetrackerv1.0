﻿using RouletteTracker.Controls;

namespace RouletteTracker.Views
{
    partial class UscColumnOverview
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lwColumnOverview = new LwColumnOverview();
            this.col1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.col2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.col3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.SuspendLayout();
            // 
            // lwColumnOverview
            // 
            this.lwColumnOverview.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.col1,
            this.col2,
            this.col3});
            this.lwColumnOverview.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lwColumnOverview.GridLines = true;
            this.lwColumnOverview.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.lwColumnOverview.Location = new System.Drawing.Point(0, 0);
            this.lwColumnOverview.Name = "lwColumnOverview";
            this.lwColumnOverview.Size = new System.Drawing.Size(198, 335);
            this.lwColumnOverview.TabIndex = 43;
            this.lwColumnOverview.UseCompatibleStateImageBehavior = false;
            this.lwColumnOverview.View = System.Windows.Forms.View.Details;
            // 
            // col1
            // 
            this.col1.Text = "Kol. 1";
            this.col1.Width = 64;
            // 
            // col2
            // 
            this.col2.Text = "Kol. 2";
            this.col2.Width = 64;
            // 
            // col3
            // 
            this.col3.Text = "Kol. 3";
            this.col3.Width = 64;
            // 
            // UscColumnOverview
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lwColumnOverview);
            this.DoubleBuffered = true;
            this.Name = "UscColumnOverview";
            this.Size = new System.Drawing.Size(198, 335);
            this.ResumeLayout(false);

        }

        #endregion

        private LwColumnOverview lwColumnOverview;
        private System.Windows.Forms.ColumnHeader col1;
        private System.Windows.Forms.ColumnHeader col2;
        private System.Windows.Forms.ColumnHeader col3;

    }
}
