﻿using System.Collections.Generic;
using System.Windows.Forms;
using RouletteTracker.Core.Views;

namespace RouletteTracker.Views
{
    public partial class UscDozenOverview : UserControl, IV_DozenOverview
    {
        public UscDozenOverview()
        {
            InitializeComponent();
        }

        public IList<int> NumberSequence
        {
            set
            {
                ResetView();
                DrawNumberSequence(value);
            }
        }

        public void AddNumber(int number)
        {
            lwDozenOverview.BeginUpdate();
            DrawNumber(number);
            lwDozenOverview.EndUpdate();
        }

        private void DrawNumberSequence(IList<int> numberSequence)
        {
            lwDozenOverview.BeginUpdate();
            for (int n = 0; n < numberSequence.Count; n++)
            {
                DrawNumber(numberSequence[n]);
            }
            lwDozenOverview.EndUpdate();
        }

        private void DrawNumber(int number)
        {
            if (number == 0) return;

            ListViewItem lwItem = new ListViewItem();

            if ((number >= 1) && (number <= 12))
            {
                lwItem.Text = "X";
                lwItem.SubItems.Add("");
                lwItem.SubItems.Add("");
            }
            else if ((number >= 13) && (number <= 24))
            {
                lwItem.Text = "";
                lwItem.SubItems.Add("X");
                lwItem.SubItems.Add("");
            }
            else if ((number >= 25) && (number <= 36))
            {
                lwItem.Text = "";
                lwItem.SubItems.Add("");
                lwItem.SubItems.Add("X");
            }

            if (lwDozenOverview.Items.Count == 0)
                lwDozenOverview.Items.Add(lwItem);
            else
                lwDozenOverview.Items.Insert(0, lwItem);
        }

        public void ResetView()
        {
            lwDozenOverview.BeginUpdate();
            lwDozenOverview.Items.Clear();
            lwDozenOverview.EndUpdate();
        }
    }
}
