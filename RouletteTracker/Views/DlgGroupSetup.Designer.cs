﻿using RouletteTracker.Controls;

namespace RouletteTracker.Views
{
    partial class DlgGroupSetup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnAddGreen = new System.Windows.Forms.Button();
            this.btnAddRed = new System.Windows.Forms.Button();
            this.btnAddBlack = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.lwGroupBlack = new LwCustom();
            this.lwGroupGreen = new LwCustom();
            this.lwGroupRed = new LwCustom();
            this.SuspendLayout();
            // 
            // btnAddGreen
            // 
            this.btnAddGreen.Location = new System.Drawing.Point(137, 16);
            this.btnAddGreen.Name = "btnAddGreen";
            this.btnAddGreen.Size = new System.Drawing.Size(75, 23);
            this.btnAddGreen.TabIndex = 3;
            this.btnAddGreen.Text = "Legg til...";
            this.btnAddGreen.UseVisualStyleBackColor = true;
            this.btnAddGreen.Click += new System.EventHandler(this.btnAddGreen_Click);
            // 
            // btnAddRed
            // 
            this.btnAddRed.Location = new System.Drawing.Point(12, 16);
            this.btnAddRed.Name = "btnAddRed";
            this.btnAddRed.Size = new System.Drawing.Size(75, 23);
            this.btnAddRed.TabIndex = 4;
            this.btnAddRed.Text = "Legg til...";
            this.btnAddRed.UseVisualStyleBackColor = true;
            this.btnAddRed.Click += new System.EventHandler(this.btnAddRed_Click);
            // 
            // btnAddBlack
            // 
            this.btnAddBlack.Location = new System.Drawing.Point(262, 16);
            this.btnAddBlack.Name = "btnAddBlack";
            this.btnAddBlack.Size = new System.Drawing.Size(76, 23);
            this.btnAddBlack.TabIndex = 5;
            this.btnAddBlack.Text = "Legg til...";
            this.btnAddBlack.UseVisualStyleBackColor = true;
            this.btnAddBlack.Click += new System.EventHandler(this.btnAddBlack_Click);
            // 
            // btnSave
            // 
            this.btnSave.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnSave.Location = new System.Drawing.Point(321, 346);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(55, 23);
            this.btnSave.TabIndex = 6;
            this.btnSave.Text = "Lagre";
            this.btnSave.UseVisualStyleBackColor = true;
            // 
            // lwGroupBlack
            // 
            this.lwGroupBlack.ForeColor = System.Drawing.Color.Black;
            this.lwGroupBlack.Location = new System.Drawing.Point(262, 45);
            this.lwGroupBlack.Name = "lwGroupBlack";
            this.lwGroupBlack.Size = new System.Drawing.Size(110, 295);
            this.lwGroupBlack.TabIndex = 2;
            this.lwGroupBlack.UseCompatibleStateImageBehavior = false;
            this.lwGroupBlack.View = System.Windows.Forms.View.List;
            // 
            // lwGroupGreen
            // 
            this.lwGroupGreen.ForeColor = System.Drawing.Color.Lime;
            this.lwGroupGreen.Location = new System.Drawing.Point(137, 45);
            this.lwGroupGreen.Name = "lwGroupGreen";
            this.lwGroupGreen.Size = new System.Drawing.Size(110, 295);
            this.lwGroupGreen.TabIndex = 1;
            this.lwGroupGreen.UseCompatibleStateImageBehavior = false;
            this.lwGroupGreen.View = System.Windows.Forms.View.List;
            // 
            // lwGroupRed
            // 
            this.lwGroupRed.ForeColor = System.Drawing.Color.Red;
            this.lwGroupRed.Location = new System.Drawing.Point(12, 45);
            this.lwGroupRed.Name = "lwGroupRed";
            this.lwGroupRed.Size = new System.Drawing.Size(110, 295);
            this.lwGroupRed.TabIndex = 0;
            this.lwGroupRed.UseCompatibleStateImageBehavior = false;
            this.lwGroupRed.View = System.Windows.Forms.View.List;
            // 
            // DlgGroupSetup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(384, 375);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnAddRed);
            this.Controls.Add(this.btnAddGreen);
            this.Controls.Add(this.lwGroupBlack);
            this.Controls.Add(this.lwGroupGreen);
            this.Controls.Add(this.lwGroupRed);
            this.Controls.Add(this.btnAddBlack);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "DlgGroupSetup";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Oppsett av nummergrupper";
            this.ResumeLayout(false);

        }

        #endregion

        private LwCustom lwGroupRed;
        private LwCustom lwGroupGreen;
        private LwCustom lwGroupBlack;
        private System.Windows.Forms.Button btnAddGreen;
        private System.Windows.Forms.Button btnAddRed;
        private System.Windows.Forms.Button btnAddBlack;
        private System.Windows.Forms.Button btnSave;
    }
}