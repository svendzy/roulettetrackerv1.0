﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using RouletteTracker.Core.Objects;
using RouletteTracker.Core.Views;

namespace RouletteTracker.Views
{
    public partial class DlgGroupSetup : Form, IV_GroupSetup
    {
        private NumberGroups _numberGroups;

        public DlgGroupSetup()
        {
            InitializeComponent();
        }

        public NumberGroups NumberGroups
        {
            get
            {
                return _numberGroups;
            }
            set
            {
                _numberGroups = value;
                RedrawListViewItems(lwGroupRed, _numberGroups.RedNumbers);
                RedrawListViewItems(lwGroupGreen, _numberGroups.GreenNumbers);
                RedrawListViewItems(lwGroupBlack, _numberGroups.BlackNumbers);
            }
        }

        private static void RedrawListViewItems(ListView listView, List<int> numbers)
        {
            listView.BeginUpdate();
            listView.Items.Clear();

            for (int nNumber = 0; nNumber < numbers.Count; nNumber++)
            {
                listView.Items.Add(Convert.ToString(numbers[nNumber]));
            }
            listView.EndUpdate();
        }

        private void btnAddRed_Click(object sender, System.EventArgs e)
        {
            DlgGroupNumber dlgGroupNumber = new DlgGroupNumber();
            DialogResult dlgResult = dlgGroupNumber.ShowDialog();
            if (dlgResult == DialogResult.OK)
            {
                int number = dlgGroupNumber.Number;

                if (!_numberGroups.RedNumbers.Contains(number))
                {
                    _numberGroups.RedNumbers.Add(number);
                    lwGroupRed.Items.Add(Convert.ToString(number));
                    _numberGroups.GreenNumbers.Remove(number);
                    _numberGroups.BlackNumbers.Remove(number);
                    RedrawListViewItems(lwGroupGreen, _numberGroups.GreenNumbers);
                    RedrawListViewItems(lwGroupBlack, _numberGroups.BlackNumbers);
                }
            }
        }

        private void btnAddGreen_Click(object sender, System.EventArgs e)
        {
            DlgGroupNumber dlgGroupNumber = new DlgGroupNumber();
            DialogResult dlgResult = dlgGroupNumber.ShowDialog();
            if (dlgResult == System.Windows.Forms.DialogResult.OK)
            {
                int number = dlgGroupNumber.Number;
                if (!_numberGroups.GreenNumbers.Contains(number))
                {
                    _numberGroups.GreenNumbers.Add(number);
                    lwGroupGreen.Items.Add(Convert.ToString(number));
                    _numberGroups.RedNumbers.Remove(number);
                    _numberGroups.BlackNumbers.Remove(number);
                    RedrawListViewItems(lwGroupRed, _numberGroups.RedNumbers);
                    RedrawListViewItems(lwGroupBlack, _numberGroups.BlackNumbers);
                }
            }
        }

        private void btnAddBlack_Click(object sender, System.EventArgs e)
        {
            DlgGroupNumber dlgGroupNumber = new DlgGroupNumber();
            DialogResult dlgResult = dlgGroupNumber.ShowDialog();
            if (dlgResult == System.Windows.Forms.DialogResult.OK)
            {
                int number = dlgGroupNumber.Number;
                if (!_numberGroups.BlackNumbers.Contains(number))
                {
                    _numberGroups.BlackNumbers.Add(number);
                    lwGroupBlack.Items.Add(Convert.ToString(number));
                    _numberGroups.RedNumbers.Remove(number);
                    _numberGroups.GreenNumbers.Remove(number);
                    RedrawListViewItems(lwGroupRed, _numberGroups.RedNumbers);
                    RedrawListViewItems(lwGroupGreen, _numberGroups.GreenNumbers);
                }
            }
        }

        public void Display()
        {
            ShowDialog();
        }

        public event EventHandler Save
        {
            add { btnSave.Click += value; }
            remove { btnSave.Click -= value; }
        }
    }
}
