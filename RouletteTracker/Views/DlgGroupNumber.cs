﻿using System;
using System.Windows.Forms;

namespace RouletteTracker.Views
{
    public partial class DlgGroupNumber : Form
    {
        public DlgGroupNumber()
        {
            InitializeComponent();
        }

        public int Number
        {
            get
            {
                return Convert.ToInt32(nuNumber.Value);
            }
        }
    }
}
